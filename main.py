import logging
from fastapi import FastAPI, Depends, HTTPException, Body
from fastapi.middleware.cors import CORSMiddleware
from models import Text, Summary, Source, Brief, SourceCreate
from database import create_db_and_tables, get_session
from sqlmodel import select
from sqlmodel.ext.asyncio.session import AsyncSession
import httpx
import os
import datetime
from dotenv import load_dotenv
from llm.interface import LLMInterface
from sqlalchemy import and_
from typing import Annotated


import nltk
nltk.download('punkt_tab')


logging.basicConfig()
logger = logging.getLogger('sqlalchemy.engine')
logger.setLevel(logging.DEBUG)


load_dotenv()

test = os.getenv('TEST')

app = FastAPI()
llm_interface = LLMInterface()

origins = ["http://127.0.0.1:8002", "http://localhost:63342", "http://localhost:8002"]

if os.environ.get('DEBUG'):
    origins.append("*")

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
async def on_startup(session: AsyncSession = Depends(get_session)):
    await create_db_and_tables()


async def get_client():
    async with httpx.AsyncClient(timeout=None) as client:
        yield client


@app.get("/healthcheck")
async def healthcheck():
    return {"status": "ok"}


@app.post("/summarize", response_model=Summary)
async def summarize(text: Text,
                    client: httpx.AsyncClient = Depends(get_client),
                    session: AsyncSession = Depends(get_session)):
    if os.environ.get('LLM_SOURCE') == "dummy":
        from llm.dummy import summarize as dummy_summarize
        summary = await dummy_summarize(text)
        summary = Summary(**summary.model_dump())
    else:
        summary = await llm_interface.summarize(text)
        summary = Summary(title=text.title, summary=summary, url=text.url, tags=text.tags, timestamp=text.timestamp)
        tags = await llm_interface.tag(text)
        summary.tags.extend(tags['topics'])

    session.add(summary)
    await session.commit()
    await session.refresh(summary)
    return summary


@app.get("/summaries/{id}", response_model=Summary)
async def retrieve_summary(id: int,
                           session: AsyncSession = Depends(get_session)):
    # Retrieve summary from database and return it
    db_summary = await session.get(Summary, id)
    if not db_summary:
        raise HTTPException(status_code=404, detail="Summary not found")
    return db_summary


@app.delete("/summaries/{id}/delete")
async def delete_summary(id: int,
                         session: AsyncSession = Depends(get_session)):
    # Look for url in database and remove it
    summary = await session.get(Summary, id)
    if not summary:
        raise HTTPException(status_code=404, detail="Summary not found")
    await session.delete(summary)
    await session.commit()
    return {"ok": True}


@app.get("/search")
async def search_summaries(query: str,
                           client: httpx.AsyncClient = Depends(get_client),
                           session: AsyncSession = Depends(get_session)) -> list:
    # response = await summarize_backend.semantic_search(query=query, session=session)
    response = [""]
    return response


@app.get("/summaries")
async def retrieve_all_summaries(session: AsyncSession = Depends(get_session)):
    # Retrieve summaries from database and return it
    db_summary = (await session.exec(select(Summary))).all()
    return db_summary


@app.patch("/summaries/{id}/add_tags")
async def add_tags(tags: list[str], id: int,
                   session: AsyncSession = Depends(get_session)):
    # Look for id in database and add tags
    summary = await session.get(Summary, id)
    if not summary:
        raise HTTPException(status_code=404, detail="Summary not found")
    new_tags = summary.tags
    new_tags.extend(x for x in tags if x not in new_tags)
    summary.tags = new_tags
    session.add(summary)
    await session.commit()
    await session.refresh(summary)
    return summary


@app.patch("/summaries/{id}/remove_tags")
async def remove_tags(tags: list[str], id: int,
                      session: AsyncSession = Depends(get_session)):
    # Look for url in database and remove tags
    summary = await session.get(Summary, id)
    if not summary:
        raise HTTPException(status_code=404, detail="Summary not found")
    for tag in tags:
        summary.tags.remove(tag)
    session.add(summary)
    await session.commit()
    await session.refresh(summary)
    return summary


@app.post("/sources/create")
async def create_source(source: SourceCreate,
                        session: AsyncSession = Depends(get_session)):
    source = Source(**source.model_dump())
    session.add(source)
    await session.commit()
    await session.refresh(source)
    return source


@app.patch("/sources/refresh")
async def refresh_sources(session: AsyncSession = Depends(get_session)):
    sources = (await session.exec(select(Source))).all()
    for source in sources:
        new_docs = await source.refresh()
        for doc in new_docs:
            if isinstance(doc, Text):
                summary = await llm_interface.summarize(doc)
                summary = Summary(title=doc.title, summary=summary, url=doc.url, tags=doc.tags, source=source, timestamp=doc.timestamp)
                tags = await llm_interface.tag(doc)
                summary.tags.extend(tags['topics'])
                doc = summary
            # store in database
            summary = Summary(title=doc.title, summary=doc.summary, url=doc.url, tags=doc.tags, source=source, timestamp=doc.timestamp)
            session.add(summary)
    await session.commit()
    return "ok"


@app.post("/briefs/create")
async def generate_brief(start_date: Annotated[datetime.date, Body()],
                         end_date: Annotated[datetime.date, Body()],
                         session: AsyncSession = Depends(get_session)):
    # Get summaries in time range, generate a brief consisting of topical headlines and summaries
    start_date = datetime.datetime(start_date.year, start_date.month, start_date.day)
    end_date = datetime.datetime(end_date.year, end_date.month, end_date.day, 23, 59, 59)
    # Get summaries, filtered by timestamp
    summaries = (await session.exec(select(Summary).where(and_(start_date <= Summary.timestamp,
                                                          Summary.timestamp <= end_date)))).all()
    # Get a text with summaries partitioned by topic and a list of topics
    text, topics = await llm_interface.summarize_summaries_to_brief(summaries)
    # Store brief in database
    brief = Brief(topics=topics,
                  text=text,
                  based_on=list(summaries))
    session.add(brief)
    await session.commit()
    return brief.model_dump()
