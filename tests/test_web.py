import os

from main import app, get_session
from models import Summary, Source, Brief
from database import create_db_and_tables
from sqlmodel import Session, SQLModel, select
from sqlmodel.pool import StaticPool
import pytest
import pytest_asyncio
from httpx import AsyncClient
from sqlmodel.ext.asyncio.session import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine
import datetime


import logging

logging.basicConfig()
logging.getLogger('sqlalchemy.engine.Engine').setLevel(logging.ERROR)


@pytest.fixture(name="session")
async def session_fixture() -> AsyncSession:
    engine = create_async_engine(
        "sqlite+aiosqlite:///test.db", echo=False, future=True, connect_args={"check_same_thread": False}, poolclass=StaticPool
    )
    async with engine.begin() as conn:
        await conn.run_sync(SQLModel.metadata.drop_all)
        await conn.run_sync(SQLModel.metadata.create_all)
    async with AsyncSession(engine) as session:
        yield session


@pytest.fixture(name="client")
async def client_fixture(session: Session):
    def get_session_override():
        return session

    app.dependency_overrides[get_session] = get_session_override

    async with AsyncClient(app=app, base_url="http://test") as client:
        yield client
    app.dependency_overrides.clear()


async def test_summarize_webpage(client: AsyncClient):
    data = {'title': "A 'middle income' or profitability trap?",
            'url': 'https://thenextrecession.wordpress.com/2024/08/17/a-middle-income-or-profitability-trap/',
            'content': '',
            'tags': []}
    if os.environ["LLM_SOURCE"] == 'dummy':
        data['content'] = 'Test content'
        data['tags'] = ['test tag 1', 'test tag 2']
    response = await client.post("/summarize", json=data)
    assert response.status_code == 200
    data = response.json()
    assert data['title']
    assert data['url']
    assert data['summary']
    assert data['tags']


async def test_summarize_youtube(client: AsyncClient):
    data = {'title': "How might LLMs store facts?",
            'url': 'https://www.youtube.com/watch?v=9-Jl0dxWQs8',
            'content': '',
            'tags': []}
    if os.environ["LLM_SOURCE"] == 'dummy':
        return
    response = await client.post("/summarize", json=data)
    assert response.status_code == 200
    data = response.json()
    assert data['title']
    assert data['url']
    assert data['summary']
    assert data['tags']


async def test_summarize_pdf(client: AsyncClient):
    data = {'title': "Multiway spectral community detection in networks",
            'url': 'https://zero.sci-hub.st/5814/c808f5f8c7646e81be270c0c9ac40335/zhang2015.pdf',
            'content': '',
            'tags': []}
    if os.environ["LLM_SOURCE"] == 'dummy':
        return
    response = await client.post("/summarize", json=data)
    assert response.status_code == 200
    data = response.json()
    assert data['title']
    assert data['url']
    assert data['summary']
    assert data['tags']


async def test_search(client: AsyncClient):
    response = await client.get("/search?query=bananer")
    assert response.status_code == 200


async def test_retrieve(client: AsyncClient, session: AsyncSession):
    s = Summary(title="Test title", url="Test URL", summary="Mock summary", tags=["Test tag 1", "Test tag 2"])
    session.add(s)
    await session.commit()

    response = await client.get("/summaries/1")

    assert response.status_code == 200
    data = response.json()

    if os.environ['LLM_SOURCE'] == 'dummy':
        assert data['title'] == 'Test title'
        assert data['url'] == 'Test URL'
        assert data['summary'] == 'Mock summary'
        assert data['tags'] == ['Test tag 1', 'Test tag 2']
    else:
        assert data['title']
        assert data['url']
        assert data['summary']
        assert data['tags']


async def test_delete(client: AsyncClient, session: AsyncSession):
    s = Summary(title="Test title", url="Test URL", summary="Mock summary", tags=["Test tag 1", "Test tag 2"])
    session.add(s)
    await session.commit()

    response = await client.delete("/summaries/1/delete")
    assert response.status_code == 200
    assert response.json() == {'ok': True}

    db_summary = await session.get(Summary, 1)
    assert db_summary is None


async def test_retrieve_all(client: AsyncClient, session: AsyncSession):
    a = Summary(title="Test title", url="Test URL", summary="This is a mock summary of a longer text.", tags=["Test tag 1", "Test tag 2"])
    b = Summary(title="Test title 2", url="Test URL 2", summary="This is a mock summary of a longer text.", tags=["Test tag 2"])
    session.add(a)
    session.add(b)
    await session.commit()

    response = await client.get("/summaries")
    assert response.status_code == 200
    data = response.json()
    assert data[0]['title'] == 'Test title'
    assert data[0]['url'] == 'Test URL'
    assert data[0]['summary'] == 'This is a mock summary of a longer text.'
    assert data[0]['tags'] == ['Test tag 1', 'Test tag 2']
    assert data[1]['title'] == 'Test title 2'
    assert data[1]['url'] == 'Test URL 2'
    assert data[1]['summary'] == 'This is a mock summary of a longer text.'
    assert data[1]['tags'] == ['Test tag 2']


async def test_add_tags(client: AsyncClient, session: AsyncSession):
    s = Summary(title="Test title", url="Test URL", summary="Mock summary", tags=["Test tag 1", "Test tag 2"])
    session.add(s)
    await session.commit()

    response = await client.patch("/summaries/1/add_tags",
                                  json=['Test tag 3', 'Test tag 4'])

    assert response.status_code == 200
    data = response.json()
    assert data['title'] == 'Test title'
    assert data['url'] == 'Test URL'
    assert data['summary'] == 'Mock summary'
    assert data['tags'] == ['Test tag 1', 'Test tag 2', 'Test tag 3', 'Test tag 4']


async def test_remove_tags(client: AsyncClient, session: AsyncSession):
    s = Summary(title="Test title", url="Test URL", summary="Mock summary", tags=["Test tag 1", "Test tag 2"])
    session.add(s)
    await session.commit()

    response = await client.patch("/summaries/1/remove_tags",
                                  json=['Test tag 1', 'Test tag 2'])

    assert response.status_code == 200
    data = response.json()
    assert data['title'] == 'Test title'
    assert data['url'] == 'Test URL'
    assert data['summary'] == 'Mock summary'
    assert data['tags'] == []


async def test_create_rss_source(client: AsyncClient, session: AsyncSession):
    data = {
        "name": "Tanketourettes",
        "type": "rss",
        "rss_feed_url": "http://www.tanketourettes.se/rss"
    }
    response = await client.post("/sources/create", json=data)

    assert response.status_code == 200
    data = response.json()
    assert data['name'] == 'Tanketourettes'
    assert data['type'] == 'rss'
    assert data['rss_feed_url'] == 'http://www.tanketourettes.se/rss'
    assert data['last_refreshed']


async def test_refresh_rss_source(client: AsyncClient, session: AsyncSession):
    session.add(Source(name="BBC",
                       type="rss",
                       rss_feed_url="https://feeds.bbci.co.uk/news/rss.xml"))
    await session.commit()
    response = await client.patch("/sources/refresh")
    assert response.status_code == 200
    sources = (await session.exec(select(Source))).all()
    for source in sources:
        if source.type == "rss":
            summaries = await source.refresh()
            assert summaries
            await session.refresh(source)
            assert source.summaries


async def test_create_brief(client: AsyncClient, session: AsyncSession):
    if os.environ["LLM_SOURCE"] == "dummy":
        return
    session.add(Source(name="BBC",
                       type="rss",
                       rss_feed_url="https://feeds.bbci.co.uk/news/rss.xml"))
    await session.commit()
    await client.patch("/sources/refresh")
    start_date = (datetime.date.today() - datetime.timedelta(10)).isoformat()
    end_date = datetime.date.today().isoformat()
    await client.post("/briefs/create", json={'start_date': start_date, 'end_date': end_date})
    briefs = (await session.exec(select(Brief))).all()
    assert briefs
