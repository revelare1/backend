from sqlmodel import SQLModel
import os
from sqlmodel.ext.asyncio.session import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.ext.asyncio import async_sessionmaker
# from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv
import time


load_dotenv()

if os.getenv("TEST"):
    os.environ["DATABASE_URL"] = "sqlite+aiosqlite:///test.db"

DATABASE_URL = os.environ.get("DATABASE_URL")

while True:
    try:
        engine = create_async_engine(DATABASE_URL, echo=True, future=True)
        break
    except ConnectionRefusedError:
        time.sleep(10)


async def create_db_and_tables():
    print(engine.url)
    async with engine.begin() as conn:
        await conn.run_sync(SQLModel.metadata.create_all)
    from models import Source
    async with AsyncSession(engine) as session:
        session.add(Source(name="User Input", type="user_input"))
        await session.commit()


async def get_session() -> AsyncSession:
    async_session = async_sessionmaker(
        bind=engine, class_=AsyncSession, expire_on_commit=False
    )
    async with async_session() as session:
        yield session
