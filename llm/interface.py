import logging
import os
import tempfile
import httpx
from langchain.chains.llm import LLMChain
from langchain.chains.combine_documents.stuff import StuffDocumentsChain
from langchain_core.documents.base import Document
from langchain_core.prompts import PromptTemplate
from langchain_core.prompts import ChatPromptTemplate
# from langchain_core.pydantic_v1 import BaseModel, Field
from pydantic import BaseModel, Field
from langchain_community.llms.llamafile import Llamafile
from langchain_community.document_loaders import AsyncChromiumLoader
from langchain_community.document_transformers import Html2TextTransformer
from langchain_community.document_loaders import YoutubeLoader
from langchain_community.document_loaders.youtube import TranscriptFormat
from langchain_community.document_loaders import PyPDFLoader
from langchain_community.document_loaders import UnstructuredODTLoader
from langchain_community.document_loaders import Docx2txtLoader
from langchain_experimental.text_splitter import SemanticChunker
from langchain_openai.embeddings import OpenAIEmbeddings
from langchain.chains import MapReduceDocumentsChain, ReduceDocumentsChain
from langchain_openai import ChatOpenAI
from typing import Sequence
from dotenv import load_dotenv
from models import Text, Summary

load_dotenv()
os.environ["LANGCHAIN_TRACING_V2"] = "True"
os.environ["OPENAI_API_KEY"] = "sk-VKp-SZ2iSvjtIYnOQ4QyKOb5ELlv2RGA0XpbbXi5j2T3BlbkFJk7ddTyQXlDM-9IGkOL0_11udg4XRrYDOhUXB3WWLgA"

llm_source = os.getenv('LLM_SOURCE')
html2text = Html2TextTransformer()

local_llm_url = os.getenv('LOCAL_LLM_URL', 'http://127.0.0.1:8080')


# TODO: Implement some sort of loader-chooser from this list: https://python.langchain.com/v0.2/docs/integrations/document_loaders/
# TODO: Implement this loader? https://api.python.langchain.com/en/latest/document_loaders/langchain_community.document_loaders.news.NewsURLLoader.html


class LLMInterface:
    def __init__(self):
        if llm_source == 'local':
            self.llm = Llamafile(base_url=local_llm_url, temperature=0)
        elif llm_source == 'openai':
            self.llm = ChatOpenAI(temperature=0, model="gpt-4o-mini")
        elif llm_source == 'dummy':
            self.llm = None
        else:
            raise Exception(f'Invalid LLM source: {llm_source}')

    async def tag(self, text: Text) -> dict[str, str | int]:
        if isinstance(self.llm, Llamafile):
            logging.warning(msg="structured output is not implemented for Llamafile: returning a single tag.")

            tagging_prompt = ChatPromptTemplate.from_template(
                """
            Return a single, very concise, label for the topic of this text.
            """
            )

            tagging_chain = tagging_prompt | self.llm
            text.content = await self.get_content(text)
            res = tagging_chain.invoke({"input": text.content.page_content})
            return {'topics': res}

        class Classification(BaseModel):
            sentiment: str = Field(description="The sentiment of the text")
            aggressiveness: int = Field(
                ...,
                description="describes how aggressive the statement is, the higher the number the more aggressive",
                enum=[1, 2, 3, 4, 5],
            )
            topics: list[str] = Field(description="lists the topics of the text using very concise topic labels")

        tagging_prompt = ChatPromptTemplate.from_template(
            """
        Extract the desired information from the following passage.

        Only extract the properties mentioned in the 'Classification' function.

        Passage:
        {input}
        """
        )

        llm = self.llm.with_structured_output(Classification)

        tagging_chain = tagging_prompt | llm

        text.content = await self.get_content(text)

        res = tagging_chain.invoke({"input": text.content.page_content})
        return res.dict()

    async def get_content(self, text: Text) -> Document:
        # TODO Maybe use YoutubeAudioLoader?
        if "youtube" in text.url or "youtu.be" in text.url:
            loader = YoutubeLoader.from_youtube_url(
                text.url,
                add_video_info=False,
                transcript_format=TranscriptFormat.CHUNKS,
                chunk_size_seconds=180,
            )
            docs = await loader.aload()
            return Document(page_content="\n\n".join(map(repr, docs)))

        elif ".pdf" in text.url:
            loader = PyPDFLoader(text.url, extract_images=True)
            pages = await loader.aload()
            return Document(page_content="\n\n".join(p.page_content for p in pages))

        elif ".odt" in text.url:
            with tempfile.NamedTemporaryFile() as tfile:
                async with httpx.AsyncClient() as client:
                    result = await client.get(text.url)
                tfile.write(result.content)
                tfile.flush()
                loader = UnstructuredODTLoader(tfile.name, mode="single")
            docs = await loader.aload()
            return Document(page_content="\n\n".join(p.page_content for p in docs))

        elif ".docx" in text.url:
            loader = Docx2txtLoader(text.url)
            docs = await loader.aload()
            return docs[0]

        elif not text.content and text.url:
            return await self.scrape_url(text.url)

        elif text.content:
            if isinstance(text.content, Document):
                return Document(page_content=text.content.page_content)
            elif isinstance(text.content, str):
                return Document(page_content=text.content)
            else:
                raise Exception(f"Unexpected content type {type(text.content)}")

        raise Exception("Didn't manage to convert the supplied text's content to Document")

    async def summarize(self, text: Text) -> str:
        text.content = await self.get_content(text)
        docs = await self.split_document(text.content)
        return await self.summarize_documents(docs)

    async def scrape_url(self, url: str) -> Document:
        loader = AsyncChromiumLoader([url])
        docs = await loader.aload()

        docs_transformed = html2text.transform_documents(docs)

        return docs_transformed[0]

    async def split_document(self, document: Document) -> Sequence[Document]:
        text_splitter = SemanticChunker(OpenAIEmbeddings(), breakpoint_threshold_type="standard_deviation")

        return await text_splitter.atransform_documents([document])

    async def summarize_summaries_to_brief(self, docs: list[Summary] | Sequence[Summary]) -> (str, list[str]):
        if not self.llm:
            raise Exception("No LLM specified.")
        # Get the main topics
        template = """The following is a set of summaries
                {docs}
                Based on these summaries, please give only a list of the main topics discussed, separated by commas
                Helpful Answer:"""
        topics_prompt = PromptTemplate.from_template(template)
        topics_chain = LLMChain(llm=self.llm, prompt=topics_prompt)
        topics = (await topics_chain.ainvoke({'docs': docs}))['text'].split(",")

        # Summarize per topic
        template = """The following is a set of summaries
                {docs}
                Based on these summaries, summarize only the parts relevant to the topic {topic}
                Helpful Answer:"""
        summarize_prompt = PromptTemplate.from_template(template)
        summarize_chain = LLMChain(llm=self.llm, prompt=summarize_prompt)
        summaries = {}
        for topic in topics:
            summary = await summarize_chain.ainvoke({'docs': docs, 'topic': topic})
            summaries[topic] = summary['text']

        # Aggregate a final return
        return "\n\n\n".join([f"**{topic}**\n\n{summaries[topic]}" for topic in topics]), topics

    async def summarize_documents(self, docs: Sequence[Document] | list[Document]) -> str:
        map_template = """The following is a set of documents
        {docs}
        Based on this list of docs, please summarize the main themes
        Helpful Answer:"""
        map_prompt = PromptTemplate.from_template(map_template)
        map_chain = LLMChain(llm=self.llm, prompt=map_prompt)

        reduce_template = """The following is set of summaries:
        {docs}
        Take these and distill it into a final, consolidated summary of the main themes.
        Helpful Answer:"""
        reduce_prompt = PromptTemplate.from_template(reduce_template)
        reduce_chain = LLMChain(llm=self.llm, prompt=reduce_prompt)

        combine_documents_chain = StuffDocumentsChain(
            llm_chain=reduce_chain, document_variable_name="docs"
        )

        reduce_documents_chain = ReduceDocumentsChain(
            combine_documents_chain=combine_documents_chain,
            collapse_documents_chain=combine_documents_chain,
            token_max=4000,
        )

        map_reduce_chain = MapReduceDocumentsChain(
            llm_chain=map_chain,
            reduce_documents_chain=reduce_documents_chain,
            document_variable_name="docs",
            return_intermediate_steps=False,
        )

        result = await map_reduce_chain.ainvoke(docs)

        return result["output_text"]
