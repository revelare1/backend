
import models
from sqlmodel.ext.asyncio.session import AsyncSession


async def summarize(text: models.Text) -> models.SummaryBase:
    if not text.content:
        text.content = "Mock text"
    s = "This is a mock summary of a longer text."
    to_return = models.SummaryBase(title=text.title, summary=s, url=text.url, tags=text.tags)
    print(to_return)
    return to_return


async def semantic_search(query: str, session: AsyncSession, limit: int = 10) -> list:
    return ["hej", "test", "då"]
