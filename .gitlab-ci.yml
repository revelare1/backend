stages:
  - prepare
  - build
  - test
  - deploy

variables:
  TEST_LOCAL: "false"
  TEST_OPENAI: "false"
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""
  OPENAI_API_KEY: $OPENAI_API_KEY
  TEST_IMAGE_TAG: registry.gitlab.com/revelare1/backend:test
  PUB_IMAGE_TAG: registry.gitlab.com/revelare1/backend:latest

services:
  - name: docker:dind
    alias: docker
    command: ["--tls=false"]

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - changes:
        - "**/*.md"
        - "**/*.yml"
        - "**/*.yaml"
        - "**/*.json"
      when: never

build_test_image:
  image: docker:cli
  stage: build
  needs: [test_backend]
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker image prune -a -f && docker container prune -f
    - docker build . -t $TEST_IMAGE_TAG -f ./Containerfile
    - docker push $TEST_IMAGE_TAG
  rules:
    - if: '$CI_COMMIT_BRANCH == "main" || $CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: $TEST_OPENAI == "true"
    - if: $TEST_LOCAL == "true"
    
build_backend:
  image: docker:cli
  stage: deploy
  needs: [build_test_image, test_container_dummy]
  when: on_success
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker image prune -a -f && docker container prune -f
    - docker pull $TEST_IMAGE_TAG
    - docker image tag $TEST_IMAGE_TAG $PUB_IMAGE_TAG
    - docker push $PUB_IMAGE_TAG
  rules:
    - if: '$CI_COMMIT_BRANCH == "main" || $CI_PIPELINE_SOURCE == "merge_request_event"'

test_container_dummy:
  image: docker:24.0.6-dind
  stage: test
  needs: [build_test_image]
  script:
    - apk update && apk add git
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $TEST_IMAGE_TAG
    - docker run --env LLM_SOURCE="dummy" --env OPENAI_API_KEY=$OPENAI_API_KEY --entrypoint=python $TEST_IMAGE_TAG -m pytest tests/test_web.py
  rules:
    - if: '$CI_COMMIT_BRANCH == "main" || $CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: $TEST_OPENAI == "true"
    - if: $TEST_LOCAL == "true"

test_container_openai:
  image: docker:24.0.6-dind
  stage: test
  needs: [build_test_image, test_container_dummy]
  rules:
    - if: $TEST_OPENAI == "true"
  #  - if: '$CI_COMMIT_BRANCH == "main" || $CI_PIPELINE_SOURCE == "merge_request_event"'
  script:
    - apk update && apk add git
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $TEST_IMAGE_TAG
    - docker run --env LLM_SOURCE="openai" --env OPENAI_API_KEY=$OPENAI_API_KEY --entrypoint=python $TEST_IMAGE_TAG -m pytest tests/test_web.py

test_container_local:
  image: docker:24.0.6-dind
  stage: test
  needs: [build_test_image, test_container_dummy]
  rules:
    - if: $TEST_LOCAL == "true"
  #  - if: '$CI_COMMIT_BRANCH == "main" || $CI_PIPELINE_SOURCE == "merge_request_event"'
  script:
    - apk update && apk add git && apk add curl
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker network create test_network
    - docker pull registry.gitlab.com/revelare1/llm:latest
    - docker run --name llamafile --net=test_network -d -p 8080:8080 registry.gitlab.com/revelare1/llm:latest
    - sleep 15
    - docker ps -a
    - curl http://docker:8080
    - docker pull $TEST_IMAGE_TAG
    - docker run --name backend --net=test_network --env LLM_SOURCE="local" --env LOCAL_LLM_URL="http://docker:8080" --env OPENAI_API_KEY=$OPENAI_API_KEY --entrypoint=python $TEST_IMAGE_TAG -m pytest tests/test_web.py
    - docker stop $(docker ps -a -q --filter ancestor=registry.gitlab.com/revelare1/backend:latest --format="{{.ID}}")

test_lint:
  image: python:3.10
  stage: prepare
  before_script:
    - python -V
    - python -m venv venv
    - source venv/bin/activate
  script:
    - pip install flake8 flake8-bandit
    - flake8 --statistics --ignore=S113
  rules:
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
      changes:
        paths:
          - /**/*.py
      when: on_success

test_backend:
  image: python:3.10
  needs: [test_lint]
  stage: prepare
  script:
    - python -m venv venv
    - source venv/bin/activate
    - pip install -r requirements.txt
    - export LLM_SOURCE="dummy"
    - python -m pytest --junitxml=report.xml tests/test_web.py 
  artifacts:
    when: always
    reports:
      junit: report.xml
  rules:
    - if: $CI_COMMIT_BRANCH == "dev" || $CI_COMMIT_BRANCH == "main"
      changes:
        paths:
          - /**/*.py
      when: on_success
