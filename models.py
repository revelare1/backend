from pydantic import BaseModel
from sqlmodel import Field, SQLModel, JSON, Column, Relationship
from sqlalchemy.ext.mutable import MutableList
from typing import List
from langchain_core.documents.base import Document
from langchain_community.document_loaders import RSSFeedLoader
from langchain_community.document_loaders import TwitterTweetLoader
from datetime import datetime
from pydantic import field_validator


class Text(BaseModel):
    title: str
    content: str | Document | None = None
    url: str
    timestamp: datetime | None = None
    tags: list[str] = []


class SummaryBase(SQLModel):
    title: str = Field(index=True)
    summary: str
    url: str = Field(default=None)
    timestamp: datetime | None = None
    tags: list[str] = []


class SourceCreate(BaseModel):
    name: str | None = None
    type: str
    rss_feed_url: str | None = None
    twitter_bearer_token: str | None = None
    twitter_users: str | None = None


class Source(SQLModel, table=True):
    id: int | None = Field(default=None, primary_key=True)
    name: str | None = None
    summaries: list["Summary"] = Relationship(back_populates="source",
                                              sa_relationship_kwargs=dict(lazy="selectin"))
    last_refreshed: datetime = datetime.now()
    type: str

    rss_feed_url: str | None = None
    twitter_bearer_token: str | None = None
    twitter_users: str | None = None  # Formatted like 'elonmusik,ggreenwald,bbc' TODO: Validation?

    @field_validator('type')
    @classmethod
    def type_must_be_valid_type(cls, v: str) -> str:
        if v not in ["rss", "user_input", "twitter"]:
            raise ValueError('Invalid source type')
        return v

    @field_validator('twitter_users')
    @classmethod
    def validate_twitter_users(cls, twitter_users: str) -> str:
        if twitter_users:
            if not twitter_users.split(","):
                raise ValueError('Invalid twitter users list')
        return twitter_users

    async def refresh(self) -> list[SummaryBase | Text]:
        if self.type == "rss":
            loader = RSSFeedLoader(urls=[self.rss_feed_url], nlp=True)
            data = await loader.aload()
            # check if urls that are loaded are already in db and filter those
            # or use last_refreshed to filter previously loaded urls

            self.last_refreshed = datetime.now()

            return [SummaryBase(title=page.metadata["title"],
                                summary=page.metadata["summary"],
                                url=page.metadata["link"],
                                timestamp=page.metadata["publish_date"] if page.metadata["publish_date"] else datetime.today(),
                                tags=page.metadata["keywords"]) for page in data]

        elif self.type == "twitter":
            loader = TwitterTweetLoader.from_bearer_token(
                oauth2_bearer_token=self.twitter_bearer_token,
                twitter_users=self.twitter_users.split(","),
                number_tweets=50,  # Default value is 100
            )
            documents = await loader.aload()
            # Return the tweets as a single Text so that it is summarized and stored as one Summary
            return [Text(title=f"Tweets from {self.twitter_users} at {datetime.now()}",
                         content="\n\n".join([d.page_content for d in documents]),
                         url="twitter.com")]
        else:
            return []


class SummaryBriefLink(SQLModel, table=True):
    summary_id: int | None | None = Field(default=None, foreign_key="summary.id", primary_key=True)
    brief_id: int | None | None = Field(default=None, foreign_key="brief.id", primary_key=True)


class Summary(SummaryBase, table=True):
    id: int | None = Field(default=None, primary_key=True)

    source_id: int | None = Field(default=1, foreign_key="source.id")
    source: Source = Relationship(back_populates="summaries")
    briefs: list["Brief"] = Relationship(back_populates="based_on", link_model=SummaryBriefLink)

    # See:
    # https://github.com/tiangolo/sqlmodel/issues/178#issuecomment-989908481,
    # https://github.com/tiangolo/sqlmodel/issues/468, and
    # https://amercader.net/blog/beware-of-json-fields-in-sqlalchemy/
    # Can be swapped out for Array if we're sticking to just PostgresQL
    tags: List[str] = Field(sa_column=Column(MutableList.as_mutable(JSON(none_as_null=True))), default={})

    # Needed for Column(JSON)
    class Config:
        arbitrary_types_allowed = True


class Brief(SQLModel, table=True):
    id: int | None = Field(default=None, primary_key=True)
    topics: List[str] = Field(sa_column=Column(MutableList.as_mutable(JSON(none_as_null=True))), default={})
    text: str

    based_on: list["Summary"] = Relationship(back_populates="briefs", link_model=SummaryBriefLink)

    # Needed for Column(JSON)
    class Config:
        arbitrary_types_allowed = True
